# Imagen base
FROM node:latest

#directorio de la app en el contenedor
WORKDIR /app

#copiado de archivos
ADD . /app

# dependencias
RUN npm install

#Puerto que expongo
EXPOSE 300

#comando
CMD ["npm", "start"]
